package com.example.aldhiramdan.yumlytest.ui.fragment;

import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.aldhiramdan.yumlytest.R;
import com.example.aldhiramdan.yumlytest.helper.GlideHelper;

import butterknife.BindView;

/**
 * Created by aldhiramdan on 3/31/17.
 */

public class HomeDetailContentFragment extends BaseFragment {

    public static final String TAG = HomeDetailContentFragment.class.getSimpleName();

    @BindView(R.id.iv_collaps_image)
    ImageView ivContentFood;
    @BindView(R.id.ratingBar)
    RatingBar ratingBar;
    @BindView(R.id.tv_content_food_title)
    TextView tvFoodTitle;
    @BindView(R.id.tv_content_food_description)
    TextView tvFoodDescription;
    @BindView(R.id.tv_ingredients)
    TextView tvIngridients;
    @BindView(R.id.tv_minutes)
    TextView tvMinutes;

    public static HomeDetailContentFragment newInstance() {
        return new HomeDetailContentFragment();
    }

    @Override
    protected int getFragmentView() {
        return R.layout.layout_food_content;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public void setupItem(String image, int rating, String foodTitle, String desc, String ingredient, String minutes) {
        GlideHelper.glide(getContext(), image, ivContentFood, R.drawable.ic_food);
        ratingBar.setRating(rating);
        tvFoodTitle.setText(foodTitle);
        tvFoodDescription.setText(desc);
        tvIngridients.setText(ingredient);
        tvMinutes.setText(minutes);
    }
}
