package com.example.aldhiramdan.yumlytest.ui.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.aldhiramdan.yumlytest.R;
import com.example.aldhiramdan.yumlytest.interactor.SearchPresenterImpl;
import com.example.aldhiramdan.yumlytest.listener.RecyclerOnScrollChangeListener;
import com.example.aldhiramdan.yumlytest.listener.RvOnClickListener;
import com.example.aldhiramdan.yumlytest.network.response.FoodDetailResponse;
import com.example.aldhiramdan.yumlytest.presenter.SearchPresenter;
import com.example.aldhiramdan.yumlytest.ui.adapter.HomeContentAdapter;
import com.example.aldhiramdan.yumlytest.ui.adapter.SearchAutoCompleteAdapter;
import com.example.aldhiramdan.yumlytest.view.SearchView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by aldhiramdan on 3/29/17.
 */

public class SearchActivity extends BaseActivity implements RvOnClickListener, SearchView {

    private HomeContentAdapter mAdapter;
    private SearchAutoCompleteAdapter mAtvAdapter;
    private List<FoodDetailResponse.Matches> mResult;
    private int totalItemsCount;

    private SearchPresenter mPresenter;
    private Drawable iconSearch, iconClose;

    @BindView(R.id.atv_search_keyword)
    AutoCompleteTextView atvSearchKeyword;
    @BindView(R.id.search_close_container)
    RelativeLayout rlSearchCloseContainer;
    @BindView(R.id.ll_search_result)
    View llSearchResult;
    @BindView(R.id.swipeContainer)
    SwipeRefreshLayout swipeContainer;
    @BindView(R.id.rv_food)
    RecyclerView rvFood;
    @BindView(R.id.btn_find)
    Button btnFind;
    @BindView(R.id.iv_btn_close)
    ImageView ivBtnClose;

    public SearchActivity() {
        // TODO
    }

    public static void navigate(Activity activity) {
        Intent intent = new Intent(activity, SearchActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initInstance();
        initView();
    }

    private void initView() {
        atvSearchKeyword.setThreshold(3);
        mAtvAdapter = new SearchAutoCompleteAdapter(getBaseContext(), R.layout.layout_search_item_suggestion);
        atvSearchKeyword.setAdapter(mAtvAdapter);
        atvSearchKeyword.setOnItemClickListener(atvClickListener);
        atvSearchKeyword.setOnEditorActionListener(atvEditorListener);

        iconSearch = getResources().getDrawable(android.R.drawable.ic_menu_search);
        iconClose = getResources().getDrawable(android.R.drawable.ic_menu_close_clear_cancel);
        atvSearchKeyword.setCompoundDrawablesWithIntrinsicBounds(null, null, iconSearch, null);
        atvSearchKeyword.setOnTouchListener(atvOnTouchListener);
        //atvSearchKeyword.addTextChangedListener();

        rvFood.setHasFixedSize(true);
        rvFood.setItemAnimator(new DefaultItemAnimator());
        rvFood.setAdapter(mAdapter);
        LinearLayoutManager mLayoutManager = new GridLayoutManager(getBaseContext(), 2);
        rvFood.setLayoutManager(mLayoutManager);
        rvFood.addOnScrollListener(mRecyclerViewOnScrollListener);

        mRecyclerViewOnScrollListener.setThresholdCount(10);
        mRecyclerViewOnScrollListener.setLayoutManager(mLayoutManager);

        btnFind.setOnClickListener(btnOnClickListener);
        ivBtnClose.setOnClickListener(btnOnClickListener);
    }

    private View.OnClickListener btnOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == btnFind) {
                searchByKeyword(atvSearchKeyword.getText().toString());
            } else if (v == ivBtnClose) {
                closeResult();
            }
        }
    };

    private void initInstance() {
        mResult = new ArrayList<>();
        mAdapter = new HomeContentAdapter(getBaseContext(), mResult);
        mAdapter.setOnClickListener(this);
        mPresenter = new SearchPresenterImpl(getBaseContext(), this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.layout_search;
    }

    @Override
    protected void onDestroy() {
        if (mPresenter != null) {
            mPresenter.cancelRequest();
        }
        super.onDestroy();
    }

    private AdapterView.OnItemClickListener atvClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Intent intent = new Intent(getBaseContext(), HomeDetailActivity.class);
            HomeDetailActivity.item = mAtvAdapter.getData(position);
            startActivity(intent);
        }
    };

    private TextView.OnEditorActionListener atvEditorListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_SEARCH || (event != null && event.getAction() == KeyEvent.ACTION_UP && event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                if (atvSearchKeyword.getText().length() > 0) {
                    atvSearchKeyword.dismissDropDown();
                    InputMethodManager in = (InputMethodManager) getBaseContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    in.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                    searchByKeyword(atvSearchKeyword.getText().toString());
                }
            }
            return true;
        }
    };

    private final View.OnTouchListener atvOnTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            final int DRAWABLE_RIGHT = 2;
            Drawable[] drawables = atvSearchKeyword.getCompoundDrawables();
            if (event.getAction() == MotionEvent.ACTION_UP && drawables != null && drawables.length >= 4) {
                if (event.getRawX() >= (atvSearchKeyword.getRight() - drawables[DRAWABLE_RIGHT].getBounds().width())) {
                    if (drawables[DRAWABLE_RIGHT] == iconSearch) {
                        if (atvSearchKeyword.getText().length() > 0) {
                            InputMethodManager in = (InputMethodManager) getBaseContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                            in.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                            searchByKeyword(atvSearchKeyword.getText().toString());
                        }
                    } else if (drawables[DRAWABLE_RIGHT] == iconClose) {
                        atvSearchKeyword.setText("");
                        atvSearchKeyword.invalidate();
                    }
                    return true;
                }
            }
            return false;
        }
    };

    private void searchByKeyword(String keyword) {
        clearResult();
        showResult();
        swipeContainer.setEnabled(false);
        swipeContainer.setRefreshing(true);
        mAdapter.setIsLoading(true);
        mRecyclerViewOnScrollListener.setIsLoading(true);
        mRecyclerViewOnScrollListener.setIsLastPage(false);
        mPresenter.searchbyKeyword(keyword);
    }

    private void clearResult() {
        mResult.clear();
        totalItemsCount = 0;
        mAdapter.notifyDataSetChanged();
        mRecyclerViewOnScrollListener.setThresholdCount(10);
    }

    private final RecyclerOnScrollChangeListener mRecyclerViewOnScrollListener = new RecyclerOnScrollChangeListener() {
        @Override
        public void loadMore(int page) {
            mRecyclerViewOnScrollListener.setIsLoading(true);
            mRecyclerViewOnScrollListener.setIsLastPage(false);
            mAdapter.setIsLoading(true);
            mPresenter.loadMoreResult(totalItemsCount);
        }

        @Override
        public void onPageUp() {

        }

        @Override
        public void onPageDown() {

        }
    };

    private void showResult() {
        Animation anim = AnimationUtils.loadAnimation(getBaseContext(), R.anim.slide_in_bottom);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                rlSearchCloseContainer.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        rlSearchCloseContainer.startAnimation(anim);
    }

    private void clear() {
        atvSearchKeyword.setText("");
        mPresenter.cancelRequest();
        mResult.clear();
        totalItemsCount = 0;
        mAdapter.notifyDataSetChanged();
        mRecyclerViewOnScrollListener.setThresholdCount(10);
    }

    private void closeResult() {
        clear();
        mPresenter.cancelRequest();
        Animation animation = AnimationUtils.loadAnimation(getBaseContext(), R.anim.slide_out_bottom);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                rlSearchCloseContainer.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        rlSearchCloseContainer.startAnimation(animation);
    }

    @Override
    public void onSearchResult(List food) {
        if (food != null) {
            totalItemsCount += food.size();
            mResult.addAll(food);
            mRecyclerViewOnScrollListener.setPagination(totalItemsCount);
            mAdapter.notifyDataSetChanged();
        }

        mAdapter.setIsLoading(false);
        swipeContainer.setEnabled(false);
        swipeContainer.setRefreshing(false);
        mRecyclerViewOnScrollListener.setIsLoading(false);
        mRecyclerViewOnScrollListener.setIsLastPage(false);
    }

    @Override
    public void onSearchFailed(Throwable t) {
        mAdapter.setIsLoading(false);
        swipeContainer.setEnabled(false);
        swipeContainer.setRefreshing(false);
        mRecyclerViewOnScrollListener.setIsLoading(false);
        mRecyclerViewOnScrollListener.setIsLastPage(false);
    }

    @Override
    public void OnClick(View view, int position) {
        //Toast.makeText(getBaseContext(), "Item Selected " + position, Toast.LENGTH_SHORT).show();
    }
}
