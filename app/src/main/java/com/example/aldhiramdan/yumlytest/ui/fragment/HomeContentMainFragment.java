package com.example.aldhiramdan.yumlytest.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.aldhiramdan.yumlytest.R;
import com.example.aldhiramdan.yumlytest.helper.util.WidgetUtil;
import com.example.aldhiramdan.yumlytest.interactor.HomeContentPresenterImpl;
import com.example.aldhiramdan.yumlytest.listener.RecyclerOnScrollChangeListener;
import com.example.aldhiramdan.yumlytest.listener.RvOnClickListener;
import com.example.aldhiramdan.yumlytest.network.response.FoodDetailResponse;
import com.example.aldhiramdan.yumlytest.presenter.HomeContentPresenter;
import com.example.aldhiramdan.yumlytest.ui.activity.HomeDetailActivity;
import com.example.aldhiramdan.yumlytest.ui.adapter.HomeContentAdapter;
import com.example.aldhiramdan.yumlytest.view.HomeContentView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by aldhiramdan on 3/16/17.
 */

public class HomeContentMainFragment extends BaseFragment implements HomeContentView, RvOnClickListener {

    public static final String TAG = HomeContentMainFragment.class.getSimpleName();

    private HomeContentPresenter mPresenter;
    private HomeContentAdapter mAdapter;
    private List<FoodDetailResponse.Matches> foodItem;

    private boolean isPullToRefresh = true;
    private int rowItem = 10;
    private int totalItemsCount;

    @BindView(R.id.rv_food)
    RecyclerView rvContentFood;
    @BindView(R.id.swipeContainer)
    SwipeRefreshLayout swipeContainer;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initInstance();
    }

    @Override
    protected int getFragmentView() {
        return R.layout.layout_content_food;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onDestroy() {
        mPresenter.onDestroy();
        super.onDestroy();
    }

    private void initInstance() {
        foodItem = new ArrayList<>();
        mAdapter = new HomeContentAdapter(getActivity().getBaseContext(), foodItem);
        mAdapter.setOnClickListener(this);
        mPresenter = new HomeContentPresenterImpl(getActivity().getBaseContext(), this);
    }

    private void setupRecyclerView() {
        rvContentFood.setHasFixedSize(true);
        rvContentFood.setItemAnimator(new DefaultItemAnimator());
        rvContentFood.setAdapter(mAdapter);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity().getBaseContext(), LinearLayoutManager.VERTICAL, false);
        mRecyclerOnScrollChangeListener.setThresholdCount(10);
        mRecyclerOnScrollChangeListener.setLayoutManager(layoutManager);

        rvContentFood.setLayoutManager(layoutManager);
        rvContentFood.addOnScrollListener(mRecyclerOnScrollChangeListener);
        mPresenter.getFoodDetail(totalItemsCount, rowItem);
    }

    private final RecyclerOnScrollChangeListener mRecyclerOnScrollChangeListener = new RecyclerOnScrollChangeListener() {
        @Override
        public void loadMore(int page) {
            isPullToRefresh = false;
            mAdapter.setIsLoading(true);
            mRecyclerOnScrollChangeListener.setIsLoading(true);
            mPresenter.getFoodDetail(page, 10);
        }

        @Override
        public void onPageUp() {

        }

        @Override
        public void onPageDown() {

        }
    };

    private void initView() {
        initSwipeToRefresh();
        setupInitialSwipeRefresh();
        setupRecyclerView();
        setupInitialFoods();
    }

    private void clearFoods() {
        if (foodItem != null) {
            foodItem.clear();
            mAdapter.notifyDataSetChanged();
        }
    }

    private void initSwipeToRefresh() {
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isPullToRefresh = true;
                clearFoods();
                setupInitialFoods();
            }
        });
        WidgetUtil.swipeRefresh(swipeContainer);
    }

    private void setupInitialSwipeRefresh() {
        swipeContainer.setEnabled(true);
        swipeContainer.post(new Runnable() {
            @Override
            public void run() {
                swipeContainer.setRefreshing(true);
            }
        });
    }

    private void setupInitialFoods() {
        clearFoods();
        totalItemsCount = 0;
        mRecyclerOnScrollChangeListener.setIsLoading(true);
        mRecyclerOnScrollChangeListener.loadMore(10);
        mPresenter.getFoodDetail(totalItemsCount, 10);
    }

    private void appendUI(List<FoodDetailResponse.Matches> foods) {
        foodItem.addAll(foods);
        if (isPullToRefresh) {
            mAdapter.notifyDataSetChanged();
        } else {
            mAdapter.notifyItemChanged(foods.size() + 1, foods.size());
        }
    }

    @Override
    public void onGetFoodDetailSuccess(List<FoodDetailResponse.Matches> response) {
        mAdapter.setIsLoading(false);
        swipeContainer.setRefreshing(false);
        foodItem.addAll(response);
        mAdapter.notifyDataSetChanged();
        mRecyclerOnScrollChangeListener.setIsLastPage(false);
        mRecyclerOnScrollChangeListener.setIsLoading(false);
        totalItemsCount += foodItem.size();
        mRecyclerOnScrollChangeListener.setPagination(totalItemsCount);
    }

    @Override
    public void onGetFoodDetailFailure(Throwable throwable) {

    }

    @Override
    public void OnClick(View view, int position) {
        Intent intent = new Intent(getActivity().getBaseContext(), HomeDetailActivity.class);
        HomeDetailActivity.item = mAdapter.getSelectedItem(position);
        startActivity(intent);
    }
}
