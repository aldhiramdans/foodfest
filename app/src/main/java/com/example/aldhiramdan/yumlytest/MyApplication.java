package com.example.aldhiramdan.yumlytest;

import android.app.Application;
import android.content.Context;

/**
 * Created by aldhiramdan on 4/3/17.
 */

public class MyApplication extends Application {

    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
    }

    public static Context getContext() {
        return context;
    }
}
