package com.example.aldhiramdan.yumlytest.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.aldhiramdan.yumlytest.R;
import com.example.aldhiramdan.yumlytest.helper.GlideHelper;
import com.example.aldhiramdan.yumlytest.listener.RvOnClickListener;
import com.example.aldhiramdan.yumlytest.model.ExploreItemModel;

import java.util.List;

/**
 * Created by aldhiramdan on 3/23/17.
 */

public class ExploreAdapter extends RecyclerView.Adapter<ExploreViewHolder> {

    private List<ExploreItemModel> itemModel;
    private RvOnClickListener mListener;
    private Context mContext;

    public ExploreAdapter(Context context, List<ExploreItemModel> model) {
        this.itemModel = model;
        this.mContext = context;
    }

    public ExploreItemModel getSelectedItem(int position) {
        return itemModel.get(position);
    }

    @Override
    public ExploreViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_explore_item, parent, false);
        ExploreViewHolder viewHolder = new ExploreViewHolder(view);
        viewHolder.onClickListener(setOnClickListener());
        return viewHolder;
    }

    public void setOnClickListener(RvOnClickListener listener) {
        this.mListener = listener;
    }

    public RvOnClickListener setOnClickListener() {
        return new RvOnClickListener() {
            @Override
            public void OnClick(View view, int position) {
                mListener.OnClick(view, position);
            }
        };
    }

    @Override
    public void onBindViewHolder(ExploreViewHolder holder, int position) {
        try {
            ExploreItemModel item = itemModel.get(position);
            if (item != null) {
                holder.tvExploreItemTitle.setText(item.getTitleItem());
                holder.ivExploreItem.setImageDrawable(item.getImageItem());
                holder.ivTitleImageLogo.setImageDrawable(item.getLogoItem());
            } else {
                holder.ivExploreItem.setImageResource(R.drawable.ic_food);
                holder.ivTitleImageLogo.setImageResource(R.drawable.ic_tomato);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return itemModel != null ? itemModel.size() : 0;
    }
}
