package com.example.aldhiramdan.yumlytest.ui.fragment;

import android.widget.TextView;

import com.example.aldhiramdan.yumlytest.R;

import butterknife.BindView;

/**
 * Created by aldhiramdan on 3/31/17.
 */

public class HomeDetailDescriptionFragment extends BaseFragment {

    public static final String TAG = HomeDetailDescriptionFragment.class.getSimpleName();

    public static HomeDetailDescriptionFragment newInstance() {
        HomeDetailDescriptionFragment fragment = new HomeDetailDescriptionFragment();
        return fragment;
    }

    public HomeDetailDescriptionFragment() {
        //TODO
    }

    @BindView(R.id.tv_ingredients_value)
    TextView tvIngredientsValue;

    @Override
    protected int getFragmentView() {
        return R.layout.layout_food_description;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public void setupItem(String ingredients) {
        tvIngredientsValue.setText(ingredients);
    }

    public void appendItem(String ingredients, int number) {
        tvIngredientsValue.setText(tvIngredientsValue.getText() + "- " + ingredients + "\n\n");
    }
}
