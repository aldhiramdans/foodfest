package com.example.aldhiramdan.yumlytest.ui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.aldhiramdan.yumlytest.R;
import com.example.aldhiramdan.yumlytest.helper.util.StringUtil;
import com.example.aldhiramdan.yumlytest.network.RestHelper;
import com.example.aldhiramdan.yumlytest.network.RestService;
import com.example.aldhiramdan.yumlytest.network.response.FoodDetailResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

/**
 * Created by aldhiramdan on 3/29/17.
 */

public class SearchAutoCompleteAdapter extends ArrayAdapter<String> implements Filterable {

    private final RestService mService = RestHelper.Companion.getInstance().service();
    private Context mContext;
    private int mResourceId;
    private List<FoodDetailResponse.Matches> resultList;
    private String keyword;


    public SearchAutoCompleteAdapter(@NonNull Context context, @LayoutRes int resource) {
        super(context, resource);
        mContext = context;
        mResourceId = resource;
        resultList = new ArrayList<>();
    }

    public FoodDetailResponse.Matches getData(int position) {
        return resultList.get(position);
    }

    @Override
    public int getCount() {
        return resultList.size();
    }

    @Nullable
    @Override
    public String getItem(int position) {
        FoodDetailResponse.Matches foods = resultList.get(position);
        return getDisplayText(foods);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        try {
            if (convertView == null) {
                convertView = LayoutInflater.from(mContext).inflate(mResourceId, parent, false);
            }
            FoodDetailResponse.Matches food = resultList.get(position);
            String displayText = getDisplayText(food);

            TextView tv = (TextView) convertView.findViewById(R.id.textPrediction);
            tv.setText(StringUtil.getTextWithBoldSpan(displayText, keyword));

            if (parent == null && (parent instanceof ListView)) {
                ListView lv = null;
                try {
                    lv = (ListView) parent;
                } catch (Exception e) {
                }
                if (lv != null) {
                    lv.setDivider(new ColorDrawable(Color.TRANSPARENT));
                    lv.setDividerHeight(0);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = new TextView(mContext);
        }

        final TextView item = (TextView) convertView;
        item.post(new Runnable() {
            @Override
            public void run() {
                item.setSingleLine(false);
                item.setMaxLines(2);
            }
        });

        if (parent != null && (parent instanceof ListView)) {
            ListView lv = null;
            try {
                lv = (ListView) parent;
            } catch (Exception e) {
            }
            if (lv != null) {
                lv.setDivider(new ColorDrawable(Color.TRANSPARENT));
                lv.setDividerHeight(0);
                lv.setMinimumHeight(100);
                setListViewHeightBasedOnChildren(lv, 5);
            }
        }
        return item;
    }

    @NonNull
    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    keyword = constraint.toString();
                    // Retrieve the autocomplete results.
                    List<FoodDetailResponse.Matches> list = getSearchResult(keyword);
                    // Assign the data to the FilterResults
                    if (list != null) {
                        filterResults.values = list;
                        filterResults.count = list.size();
                    }
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    resultList = (List<FoodDetailResponse.Matches>) results.values;
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        };
        return filter;
    }

    private String getDisplayText(FoodDetailResponse.Matches food) {
        StringBuffer tmp = new StringBuffer();
        if (food != null && keyword != null) {
            if (food.recipeName != null && food.recipeName.toLowerCase().contains(keyword.toLowerCase()))
                tmp.append(food.recipeName);
            else
                tmp.append(food.sourceDisplayName);

            /*if (food.location != null)
                tmp.append(" ").append(food.location);*/
        }

        return tmp.toString();
    }

    private List<FoodDetailResponse.Matches> getSearchResult(String keyword) {
        try {
            Call<FoodDetailResponse> call = mService.searchFood(keyword, 10, 0);
            FoodDetailResponse response = call.execute().body();
            return response.data;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void setListViewHeightBasedOnChildren(ListView listView, int visibleItemCount) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }
        int totalHeight = 0, totalWidth = 0;
        // int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(),
        // MeasureSpec.AT_MOST);
        // Log.e(TAG, "desired width:" + desiredWidth);
        for (int i = 0; i < visibleItemCount && i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            //Log.e(TAG, "item width:" + listItem.getMeasuredWidth());
            if (listItem.getMeasuredWidth() > totalWidth) {
                //Log.e(TAG, "max item width:" + listItem.getMeasuredWidth());
                totalWidth = listItem.getMeasuredWidth();
            }
        }
        // Log.e(TAG, "final height:" + totalHeight);
        // Log.e(TAG, "final width:" + totalWidth);
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight
                + (listView.getDividerHeight() * (visibleItemCount - 1));
        // Log.e(TAG, "height:" + params.height);
        // popupHeight = params.height;
        // popupWidth = totalWidth;
        listView.setLayoutParams(params);
        listView.requestLayout();
    }
}
