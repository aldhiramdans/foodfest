package com.example.aldhiramdan.yumlytest.network.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by aldhiramdan on 3/20/17.
 */

public class FlavorsResponse implements Parcelable{

    public FlavorsResponse(){

    }

    @SerializedName("salty")
    @Expose
    public Float salty;
    @SerializedName("sour")
    @Expose
    public Float sour;
    @SerializedName("sweet")
    @Expose
    public Float sweet;
    @SerializedName("bitter")
    @Expose
    public Float bitter;
    @SerializedName("meaty")
    @Expose
    public Float meaty;
    @SerializedName("piquant")
    @Expose
    public Float piquant;

    protected FlavorsResponse(Parcel in) {
    }

    public static final Creator<FlavorsResponse> CREATOR = new Creator<FlavorsResponse>() {
        @Override
        public FlavorsResponse createFromParcel(Parcel in) {
            return new FlavorsResponse(in);
        }

        @Override
        public FlavorsResponse[] newArray(int size) {
            return new FlavorsResponse[size];
        }
    };

    @Override
    public String toString() {
        return "FlavorsResponse{" +
                "salty=" + salty +
                ", sour=" + sour +
                ", sweet=" + sweet +
                ", bitter=" + bitter +
                ", meaty=" + meaty +
                ", piquant=" + piquant +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FlavorsResponse that = (FlavorsResponse) o;

        if (salty != null ? !salty.equals(that.salty) : that.salty != null) return false;
        if (sour != null ? !sour.equals(that.sour) : that.sour != null) return false;
        if (sweet != null ? !sweet.equals(that.sweet) : that.sweet != null) return false;
        if (bitter != null ? !bitter.equals(that.bitter) : that.bitter != null) return false;
        if (meaty != null ? !meaty.equals(that.meaty) : that.meaty != null) return false;
        return piquant != null ? piquant.equals(that.piquant) : that.piquant == null;

    }

    @Override
    public int hashCode() {
        int result = salty != null ? salty.hashCode() : 0;
        result = 31 * result + (sour != null ? sour.hashCode() : 0);
        result = 31 * result + (sweet != null ? sweet.hashCode() : 0);
        result = 31 * result + (bitter != null ? bitter.hashCode() : 0);
        result = 31 * result + (meaty != null ? meaty.hashCode() : 0);
        result = 31 * result + (piquant != null ? piquant.hashCode() : 0);
        return result;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }
}
