package com.example.aldhiramdan.yumlytest.interactor;

import android.content.Context;

import com.example.aldhiramdan.yumlytest.helper.preference.AppSharedPreference;
import com.example.aldhiramdan.yumlytest.network.RestHelper;
import com.example.aldhiramdan.yumlytest.network.RestService;
import com.example.aldhiramdan.yumlytest.network.response.FoodDetailResponse;
import com.example.aldhiramdan.yumlytest.presenter.SearchPresenter;
import com.example.aldhiramdan.yumlytest.view.SearchView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by aldhiramdan on 3/29/17.
 */

public class SearchPresenterImpl implements SearchPresenter {

    private Context mContext;
    private SearchView mView;
    private final RestService mService = RestHelper.Companion.getInstance().service();
    private Call<FoodDetailResponse> callByKeyword;
    private Call<FoodDetailResponse> callLoadMore;

    private String keyword;

    public SearchPresenterImpl(Context context, SearchView view) {
        this.mContext = context;
        this.mView = view;
    }

    @Override
    public void searchbyKeyword(String keyword) {
        this.keyword = keyword;
        callByKeyword = mService.searchFood(keyword, 10, 0);
        callByKeyword.enqueue(new Callback<FoodDetailResponse>() {
            @Override
            public void onResponse(Call<FoodDetailResponse> call, Response<FoodDetailResponse> response) {
                if (mView != null) {
                    if (response.isSuccessful() && response.body() != null) {
                        mView.onSearchResult(response.body().data);
                    }
                }
            }

            @Override
            public void onFailure(Call<FoodDetailResponse> call, Throwable t) {
                if (mView != null) {
                    mView.onSearchFailed(t);
                }
            }
        });

    }

    @Override
    public void loadMoreResult(int page) {
        callLoadMore = mService.searchFood(keyword, 10, page);

        callLoadMore.enqueue(new Callback<FoodDetailResponse>() {
            @Override
            public void onResponse(Call<FoodDetailResponse> call, Response<FoodDetailResponse> response) {
                if (mView != null) {
                    if (response.isSuccessful() && response.body() != null) {
                        mView.onSearchResult(response.body().data);
                    }
                }
            }

            @Override
            public void onFailure(Call<FoodDetailResponse> call, Throwable t) {
                if (mView != null) {
                    mView.onSearchFailed(t);
                }
            }
        });

    }

    @Override
    public void cancelRequest() {
        if (callByKeyword != null) {
            callByKeyword.cancel();
        }
    }
}
