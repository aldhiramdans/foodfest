package com.example.aldhiramdan.yumlytest.network

/**
 * Created by aldhiramdan on 3/15/17.
 */
class ApiConstant {

    companion object {
        const val BASE_URL = "https://api.yummly.com"
        const val CONNECTION_TIMEOUT: Long = 30 * 10 * 1000
        const val READ_TIMEOUT: Long = 30 * 10 * 1000

        const val FOOD_LIST = "/v1/api/recipes"
        const val FOOD_DIET = "/v1/api/recipes?recipes&allowedDiet[]=Pescetarian&allowedDiet[]=Lacto vegetarian&allowedDiet[]=Ovo vegetarian&allowedDiet[]=Vegetarian&allowedDiet[]=Vegan"
    }

}