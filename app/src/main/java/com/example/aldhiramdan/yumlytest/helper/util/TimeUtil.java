package com.example.aldhiramdan.yumlytest.helper.util;

import java.text.DecimalFormat;

/**
 * Created by aldhiramdan on 3/20/17.
 */

public class TimeUtil {

    public static String toMinutes(int second) {
        String result;
        DecimalFormat df = new DecimalFormat();
        try {
            if (second <= 60) {
                result = df.format(second / 60);
                return result;
            } else if (second >= 60) {
                result = df.format(second / 60);
                return result;
            }
            return null;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
