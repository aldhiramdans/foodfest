package com.example.aldhiramdan.yumlytest.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;

import com.example.aldhiramdan.yumlytest.R;

import butterknife.BindView;

/**
 * Created by aldhiramdan on 3/30/17.
 */

public class LoginActivity extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.btn_login_facebook)
    Button btnFbLogin;

    public static void navigate(Activity activity) {
        Intent intent = new Intent(activity, LoginActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        btnFbLogin.setOnClickListener(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    public void onClick(View v) {
        HomeContentActivity.navigate(this);
        finish();
    }
}
