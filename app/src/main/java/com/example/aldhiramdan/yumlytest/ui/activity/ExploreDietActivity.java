package com.example.aldhiramdan.yumlytest.ui.activity;

import com.example.aldhiramdan.yumlytest.R;

/**
 * Created by aldhiramdan on 4/3/17.
 */

public class ExploreDietActivity extends BaseActivity {
    @Override
    protected int getLayoutId() {
        return R.layout.layout_content_food;
    }
}
