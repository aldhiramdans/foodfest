package com.example.aldhiramdan.yumlytest.presenter;

/**
 * Created by aldhiramdan on 3/17/17.
 */

public interface HomeContentPresenter {
    void getFoodDetail(int startRow, int maxRow);

    void onDestroy();
}
