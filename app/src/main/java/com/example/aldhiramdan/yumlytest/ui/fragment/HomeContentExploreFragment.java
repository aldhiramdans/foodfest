package com.example.aldhiramdan.yumlytest.ui.fragment;

import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.aldhiramdan.yumlytest.R;
import com.example.aldhiramdan.yumlytest.listener.RvOnClickListener;
import com.example.aldhiramdan.yumlytest.model.ExploreItemModel;
import com.example.aldhiramdan.yumlytest.ui.adapter.ExploreAdapter;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by aldhiramdan on 3/23/17.
 */

public class HomeContentExploreFragment extends BaseFragment implements RvOnClickListener {

    @BindView(R.id.rv_explore)
    RecyclerView rvExplore;

    private List<ExploreItemModel> exploreItem;
    private ExploreAdapter mAdapter;

    @Override
    protected int getFragmentView() {
        return R.layout.layout_explore;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initInstance();
        loadData();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
    }

    private void initInstance() {
        exploreItem = new ArrayList<>();
        mAdapter = new ExploreAdapter(getContext(), exploreItem);
        mAdapter.setOnClickListener(this);
    }

    private void initView() {
        setupRecyclerView();
    }

    private void setupRecyclerView() {
        rvExplore.setHasFixedSize(true);
        rvExplore.setItemAnimator(new DefaultItemAnimator());
        rvExplore.setAdapter(mAdapter);
        rvExplore.setLayoutManager(new GridLayoutManager(getContext(), 2));
    }

    @Override
    public void OnClick(View view, int position) {
        Toast.makeText(getContext(), "Item Selected", Toast.LENGTH_SHORT).show();
        Log.d("TAG", "item " + position);
    }

    private void loadData() {
        TypedArray getImage = getResources().obtainTypedArray(R.array.explore_image);
        TypedArray getImageLogo = getResources().obtainTypedArray(R.array.explore_logo);
        TypedArray getTitle = getResources().obtainTypedArray(R.array.explore_title);

        for (int i = 0; i < getImage.length(); i++) {
            ExploreItemModel model = new ExploreItemModel();
            model.imageItem = getImage.getDrawable(i);
            model.logoItem = getImageLogo.getDrawable(i);
            model.titleItem = getTitle.getString(i);
            exploreItem.add(model);
        }

        getImage.recycle();
        getImageLogo.recycle();
        getTitle.recycle();

    }
}
