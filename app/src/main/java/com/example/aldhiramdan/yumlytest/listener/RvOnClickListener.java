package com.example.aldhiramdan.yumlytest.listener;

import android.view.View;

/**
 * Created by aldhiramdan on 3/17/17.
 */

public interface RvOnClickListener {

    void OnClick(View view, int position);

}
