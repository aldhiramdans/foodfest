package com.example.aldhiramdan.yumlytest.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.aldhiramdan.yumlytest.R;
import com.example.aldhiramdan.yumlytest.listener.RvOnClickListener;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by aldhiramdan on 3/23/17.
 */

public class ExploreViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.iv_explore_item)
    ImageView ivExploreItem;
    @BindView(R.id.iv_explore_title_image_logo)
    ImageView ivTitleImageLogo;
    @BindView(R.id.tv_explore_title_item)
    TextView tvExploreItemTitle;

    private RvOnClickListener mListener;

    public ExploreViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        itemView.setOnClickListener(this);
    }

    public void onClickListener(RvOnClickListener listener) {
        this.mListener = listener;
    }

    @Override
    public void onClick(View v) {
        mListener.OnClick(v, getAdapterPosition());
    }
}
