package com.example.aldhiramdan.yumlytest.network.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by aldhiramdan on 3/19/17.
 */

public class BaseRepsonses implements Parcelable {

    @SerializedName("criteria")
    @Expose
    public Criteria criteria;
    @SerializedName("facetCounts")
    @Expose
    public FaceCount faceCount;
    @SerializedName("totalMatchCount")
    @Expose
    public Long totalMatchCount;
    @SerializedName("attribution")
    public Attribution attribution;

    protected BaseRepsonses(Parcel in) {
    }

    public static final Creator<BaseRepsonses> CREATOR = new Creator<BaseRepsonses>() {
        @Override
        public BaseRepsonses createFromParcel(Parcel in) {
            return new BaseRepsonses(in);
        }

        @Override
        public BaseRepsonses[] newArray(int size) {
            return new BaseRepsonses[size];
        }
    };

    @Override
    public String toString() {
        return "BaseRepsonses{" +
                "criteria=" + criteria +
                ", faceCount=" + faceCount +
                ", totalMatchCount=" + totalMatchCount +
                ", attribution=" + attribution +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BaseRepsonses that = (BaseRepsonses) o;

        if (criteria != null ? !criteria.equals(that.criteria) : that.criteria != null)
            return false;
        if (faceCount != null ? !faceCount.equals(that.faceCount) : that.faceCount != null)
            return false;
        if (totalMatchCount != null ? !totalMatchCount.equals(that.totalMatchCount) : that.totalMatchCount != null)
            return false;
        return attribution != null ? attribution.equals(that.attribution) : that.attribution == null;

    }

    @Override
    public int hashCode() {
        int result = criteria != null ? criteria.hashCode() : 0;
        result = 31 * result + (faceCount != null ? faceCount.hashCode() : 0);
        result = 31 * result + (totalMatchCount != null ? totalMatchCount.hashCode() : 0);
        result = 31 * result + (attribution != null ? attribution.hashCode() : 0);
        return result;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public static class Criteria {

    }

    public static class FaceCount {

    }

    public static class Attribution implements Parcelable {

        public Attribution() {

        }

        @SerializedName("html")
        @Expose
        public String html;
        @SerializedName("url")
        @Expose
        public String url;
        @SerializedName("text")
        @Expose
        public String text;
        @SerializedName("logo")
        @Expose
        public String logo;

        protected Attribution(Parcel in) {
            html = in.readString();
            url = in.readString();
            text = in.readString();
            logo = in.readString();
        }

        public static final Creator<Attribution> CREATOR = new Creator<Attribution>() {
            @Override
            public Attribution createFromParcel(Parcel in) {
                return new Attribution(in);
            }

            @Override
            public Attribution[] newArray(int size) {
                return new Attribution[size];
            }
        };

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Attribution that = (Attribution) o;

            if (html != null ? !html.equals(that.html) : that.html != null) return false;
            if (url != null ? !url.equals(that.url) : that.url != null) return false;
            if (text != null ? !text.equals(that.text) : that.text != null) return false;
            return logo != null ? logo.equals(that.logo) : that.logo == null;

        }

        @Override
        public int hashCode() {
            int result = html != null ? html.hashCode() : 0;
            result = 31 * result + (url != null ? url.hashCode() : 0);
            result = 31 * result + (text != null ? text.hashCode() : 0);
            result = 31 * result + (logo != null ? logo.hashCode() : 0);
            return result;
        }

        @Override
        public String toString() {
            return "Attribution{" +
                    "html='" + html + '\'' +
                    ", url='" + url + '\'' +
                    ", text='" + text + '\'' +
                    ", logo='" + logo + '\'' +
                    '}';


        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(html);
            dest.writeString(url);
            dest.writeString(text);
            dest.writeString(logo);
        }
    }

}
