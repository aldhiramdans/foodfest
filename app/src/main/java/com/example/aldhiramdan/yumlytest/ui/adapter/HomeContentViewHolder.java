package com.example.aldhiramdan.yumlytest.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.aldhiramdan.yumlytest.R;
import com.example.aldhiramdan.yumlytest.listener.RvOnClickListener;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by aldhiramdan on 3/17/17.
 */

public class HomeContentViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


    @BindView(R.id.fl_content_food)
    LinearLayout llContentFood;
    @BindView(R.id.iv_content_food)
    ImageView ivContentFood;
    @BindView(R.id.tv_content_food_title)
    TextView tvContentFoodTitle;
    @BindView(R.id.tv_content_food_description)
    TextView tvContentFoodDescription;
    @BindView(R.id.tv_recomended)
    TextView tvRecomended;

    private RvOnClickListener setRvOnCLickListener;


    public HomeContentViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        itemView.setOnClickListener(this);
    }

    public void setOnCLickListener(RvOnClickListener listener) {
        this.setRvOnCLickListener = listener;
    }

    @Override
    public void onClick(View v) {
        setRvOnCLickListener.OnClick(v, getAdapterPosition());
    }
}
