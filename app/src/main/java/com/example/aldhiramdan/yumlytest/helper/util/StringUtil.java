package com.example.aldhiramdan.yumlytest.helper.util;

import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.StyleSpan;

import java.text.DecimalFormat;

/**
 * Created by aldhiramdan on 3/29/17.
 */

public class StringUtil {

    public static boolean isStringNotNull(String message) {
        return message != null && !TextUtils.isEmpty(message);
    }

    public static boolean isStringContainsValue(String message, int length) {
        return message != null && !TextUtils.isEmpty(message) && message.length() > length;
    }

    public static String isStringNotEmpty(String var1, String var2) {
        return (var1 != null && !TextUtils.isEmpty(var1)) ? var1 : var2;
    }

    public static String getUserNameXMPP(String username) {
        String[] name = username.split("@");
        return name[0];
    }

    public static SpannableStringBuilder getTextWithBoldSpan(String text, String spanText) {
        return getTextWithSpan(text, spanText, new StyleSpan(Typeface.BOLD), true);
    }

    public static SpannableStringBuilder getTextWithSpan(String text, String spanText, StyleSpan style, boolean fullWord) {
        SpannableStringBuilder sb = new SpannableStringBuilder(text);
        if (text != null && !text.isEmpty()) {
            int spanLen = spanText.length();
            int start = text.toLowerCase().indexOf(spanText.toLowerCase());
            if (start >= 0) {
                int end = start + spanLen;
                if (fullWord) {
                    end = text.length();
                    int spacePos = text.toLowerCase().indexOf(" ", start + spanLen);
                    if (spacePos > start)
                        end = spacePos;
                }
                sb.setSpan(style, start, end, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            }
        }
        return sb;
    }

    public static String getName(String name) {
        if (name.contains("@")) {
            String part[] = name.split("\\@");
            return part[0];
        }
        return name;
    }

    public static String getNameAndDomain(String name) {
        String[] names = name.split("\\@");
        return names[0].concat(".").concat(names[1]);
    }

    public static String getNameFromSlash(String name) {
        if (name.contains("/")) {
            String part[] = name.split("/");
            return part[0];
        }
        return name;
    }

    public static String getShortName(String name) {
        if (TextUtils.isEmpty(name))
            return "";

        String s = name;
        final int maxLen = 10;
        int len = s.length();
        int maxPos = s.indexOf(" ");
        int pos = Math.min(maxLen, maxPos);
        if (pos > 0 && pos < len)
            s = name.substring(0, pos);
        return s;
    }

    public static String getFormattedDisplayName(String displayName) {
        StringBuffer bufferName = new StringBuffer();
        String nameArray = displayName.replaceAll("[^a-zA-Z]", " ").trim();
        int i = nameArray.indexOf(" ", 3);

        if (i > 0) {
            bufferName.append(nameArray.substring(0, i));
        } else {
            bufferName.append(nameArray);
        }

        return bufferName.toString().trim();
    }

    public static String getFormattedNumber(Double distance) {
        String result = "";
        DecimalFormat dF = new DecimalFormat("00");
        dF.applyPattern("0.#");
        if (distance < 1000) {
            result = dF.format(distance) + "";
        } else if (distance > 1000) {
            distance = distance / 1000.0;
            result = dF.format(distance) + " K";
        } else if (distance > 1000000) {
            distance = distance / 1000000.0;
            result = dF.format(distance) + " M";
        } else if (distance > 1000000000) {
            distance = distance / 1000000000.0;
            result = dF.format(distance) + " B";
        }
        return result;
    }

}
