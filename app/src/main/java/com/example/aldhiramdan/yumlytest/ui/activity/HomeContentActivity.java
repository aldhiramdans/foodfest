package com.example.aldhiramdan.yumlytest.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.aldhiramdan.yumlytest.R;
import com.example.aldhiramdan.yumlytest.ui.adapter.ViewPagerAdapter;
import com.example.aldhiramdan.yumlytest.ui.fragment.HomeContentExploreFragment;
import com.example.aldhiramdan.yumlytest.ui.fragment.HomeContentMainFragment;

import butterknife.BindView;

/**
 * Created by aldhiramdan on 3/16/17.
 */

public class HomeContentActivity extends BaseActivity {

    public static final String TAG = HomeContentActivity.class.getSimpleName();

    @BindView(R.id.tb_home)
    Toolbar toolbar;
    @BindView(R.id.tabs)
    TabLayout tabLayout;
    @BindView(R.id.viewpager)
    ViewPager viewPager;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.tv_toolbar_title)
    TextView tvToolbarTitle;

    public static void navigate(Activity activity) {
        Intent intent = new Intent(activity, HomeContentActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initiew();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_home;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void initiew() {
        //setupToolbar(toolbar, tvToolbarTitle, "FoodFest");
        tvToolbarTitle.setText(getString(R.string.food_fest));
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getBaseContext(), "FAB Action", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new HomeContentMainFragment(), "FOOD");
        adapter.addFragment(new HomeContentExploreFragment(), "EXPLORE");
        viewPager.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_search) {
            SearchActivity.navigate(this);
        }
        return super.onOptionsItemSelected(item);
    }

}
