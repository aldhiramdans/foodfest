package com.example.aldhiramdan.yumlytest.ui.adapter;

import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.aldhiramdan.yumlytest.R;
import com.example.aldhiramdan.yumlytest.helper.GlideHelper;
import com.example.aldhiramdan.yumlytest.listener.RvOnClickListener;
import com.example.aldhiramdan.yumlytest.network.response.FoodDetailResponse;

import java.util.List;

/**
 * Created by aldhiramdan on 3/17/17.
 */

public class HomeContentAdapter extends RecyclerView.Adapter<HomeContentViewHolder> {

    public static final String TAG = HomeContentAdapter.class.getSimpleName();

    private List<FoodDetailResponse.Matches> foodItem;
    private RvOnClickListener mListener;
    private Context mContext;

    private Boolean isLoading;
    private int loadingViewPosition;

    public HomeContentAdapter(Context context, List<FoodDetailResponse.Matches> data) {
        this.foodItem = data;
        this.mContext = context;
    }

    public FoodDetailResponse.Matches getSelectedItem(int position) {
        return foodItem.get(position);
    }

    @Override
    public HomeContentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_list_content_food_item, parent, false);
        HomeContentViewHolder holder = new HomeContentViewHolder(view);
        holder.setOnCLickListener(setOnClickListener());
        return holder;
    }

    @Override
    public void onBindViewHolder(HomeContentViewHolder holder, int position) {
        if (holder != null) {
            try {
                FoodDetailResponse.Matches item = foodItem.get(position);
                if (item.imageUrlsBySize.ninety != null && !TextUtils.isEmpty(item.imageUrlsBySize.ninety)) {
                    GlideHelper.glide(mContext, item.imageUrlsBySize.ninety, holder.ivContentFood, R.drawable.ic_food);
                } else {
                    holder.ivContentFood.setImageResource(R.drawable.ic_food);
                }
                if (item.recipeName != null && !TextUtils.isEmpty(item.recipeName) && item.sourceDisplayName != null && !TextUtils.isEmpty(item.sourceDisplayName)) {
                    holder.tvContentFoodTitle.setText(item.recipeName);
                    holder.tvContentFoodDescription.setText(item.sourceDisplayName);
                }
                if (item.rating >= 5.0) {
                    holder.tvRecomended.setVisibility(View.VISIBLE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public int getItemCount() {
        return foodItem != null ? foodItem.size() : 0;
    }

    public void setOnClickListener(RvOnClickListener listener) {
        this.mListener = listener;
    }

    private RvOnClickListener setOnClickListener() {
        return new RvOnClickListener() {
            @Override
            public void OnClick(View view, int position) {
                mListener.OnClick(view, position);
            }
        };
    }

    public void setIsLoading(boolean isLoading) {
        this.isLoading = isLoading;
        if (isLoading) {
            if (getItemCount() > loadingViewPosition && foodItem.get(loadingViewPosition) == null) {
                foodItem.remove(loadingViewPosition);
                notifyItemRemoved(loadingViewPosition);
            }

            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (getItemCount() > 0 && foodItem.get(getItemCount() - 1) != null) {
                            foodItem.add(null);
                            loadingViewPosition = getItemCount() - 1;
                            notifyItemInserted(loadingViewPosition);
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "setIsLoading", e);
                    }
                }
            });
        } else {
            try {
                if (getItemCount() > loadingViewPosition && foodItem.get(loadingViewPosition) == null) {
                    foodItem.remove(loadingViewPosition);
                    notifyItemRemoved(loadingViewPosition);
                }
            } catch (Exception e) {
                Log.e(TAG, "setIsLoading", e);
            }
        }
    }
}
