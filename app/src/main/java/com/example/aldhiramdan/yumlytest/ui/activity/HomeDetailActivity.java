package com.example.aldhiramdan.yumlytest.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.example.aldhiramdan.yumlytest.R;
import com.example.aldhiramdan.yumlytest.helper.util.TimeUtil;
import com.example.aldhiramdan.yumlytest.network.response.FoodDetailResponse;
import com.example.aldhiramdan.yumlytest.ui.fragment.HomeDetailContentFragment;
import com.example.aldhiramdan.yumlytest.ui.fragment.HomeDetailDescriptionFragment;

import butterknife.BindView;

/**
 * Created by aldhiramdan on 3/20/17.
 */

public class HomeDetailActivity extends BaseActivity implements View.OnClickListener {

    public static FoodDetailResponse.Matches item;

    private HomeDetailDescriptionFragment mHomeDetailDescriptionFragment;
    private HomeDetailContentFragment mHomeDetailContentFragment;

    @BindView(R.id.tb_content_food_detail)
    Toolbar toolbar;
    @BindView(R.id.tv_toolbar_title)
    TextView tvToolbarTitle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.layout_detail_content_food;
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        initFragment();
        initView();
    }

    private void initView() {
        setupToolbar(toolbar, tvToolbarTitle, "Food Detail");
        try {
            if (item != null) {
                mHomeDetailContentFragment.setupItem(item.imageUrlsBySize.ninety, item.rating, item.recipeName, item.sourceDisplayName,
                        String.valueOf(item.ingredients.size()) + " Ingridients",
                        TimeUtil.toMinutes(item.totalTimeInSeconds) + " Minutes");

                int count = item.ingredients.size();
                for (int i = 0; i < count; i++) {
                    FoodDetailResponse.Matches food = item;
                    mHomeDetailDescriptionFragment.appendItem(food.ingredients.get(i), i);
                }


            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initFragment() {
        mHomeDetailDescriptionFragment = (HomeDetailDescriptionFragment) getSupportFragmentManager().findFragmentById(R.id.fm_desc);
        mHomeDetailContentFragment = (HomeDetailContentFragment) getSupportFragmentManager().findFragmentById(R.id.fm_content);
    }

    @Override
    protected void onDestroy() {
        item = null;
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {

    }
}
