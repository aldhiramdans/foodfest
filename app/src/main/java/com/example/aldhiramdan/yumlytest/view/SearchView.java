package com.example.aldhiramdan.yumlytest.view;

import java.util.List;

/**
 * Created by aldhiramdan on 3/29/17.
 */

public interface SearchView {

    void onSearchResult(List food);

    void onSearchFailed(Throwable t);

}
