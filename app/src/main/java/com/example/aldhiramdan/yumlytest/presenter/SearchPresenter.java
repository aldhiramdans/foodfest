package com.example.aldhiramdan.yumlytest.presenter;

/**
 * Created by aldhiramdan on 3/29/17.
 */

public interface SearchPresenter {

    void searchbyKeyword(String keyword);

    void loadMoreResult(int page);

    void cancelRequest();

}
