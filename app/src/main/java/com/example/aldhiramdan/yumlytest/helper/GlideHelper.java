package com.example.aldhiramdan.yumlytest.helper;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.ViewPropertyAnimation;

/**
 * Created by aldhiramdan on 3/17/17.
 */

public class GlideHelper {

    public static void glide(Context context,
                             final String url,
                             final ImageView resId,
                             final int placeholder) {
        Glide.with(context)
                .load(url)
                .centerCrop()
                .placeholder(placeholder)
                .animate(animationObject)
                .into(resId);
    }

    public static void glideNoAnimation(Context context,
                                        final String url,
                                        final ImageView resId,
                                        final int placeholder) {
        Glide.with(context)
                .load(url)
                .fitCenter()
                .dontAnimate()
                .placeholder(placeholder)
                .into(resId);
    }

    public static void glideFromAssets(@NonNull Context context,
                                       @NonNull ImageView resId,
                                       String placeholder) {
        Glide.with(context)
                .load(Uri.parse(placeholder))
                .skipMemoryCache(true)
                .into(resId);
    }

    public static void glideThumbnail(Context context,
                                      String url,
                                      final ImageView resId,
                                      final int errorPlaceHolderImage) {
        Glide.with(context)
                .load(url)
                .crossFade()
                .override(100, 100)
                .diskCacheStrategy(DiskCacheStrategy.RESULT)
                .skipMemoryCache(true)
                .error(errorPlaceHolderImage)
                .into(resId);
    }

    /**
     * For performance LAYER_TYPE_HARDWARE
     * https://futurestud.io/tutorials/glide-custom-animations-with-animate
     */
    private static ViewPropertyAnimation.Animator animationObject = new ViewPropertyAnimation.Animator() {
        @Override
        public void animate(final View view) {
            // if it's a custom view class, cast it here
            // then find subviews and do the animations
            // here, we just use the entire view for the fade animation
            view.setAlpha(0f);
            view.setLayerType(View.LAYER_TYPE_HARDWARE, null);
            ObjectAnimator fadeAnim = ObjectAnimator.ofFloat(view, "alpha", 0f, 1f);
            fadeAnim.setDuration(1000);
            fadeAnim.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    view.setLayerType(View.LAYER_TYPE_NONE, null);
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });
            fadeAnim.start();
        }
    };

}
