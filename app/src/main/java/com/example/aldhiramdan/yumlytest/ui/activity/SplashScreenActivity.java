package com.example.aldhiramdan.yumlytest.ui.activity;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;

import com.example.aldhiramdan.yumlytest.R;

/**
 * Created by aldhiramdan on 3/30/17.
 */

public class SplashScreenActivity extends BaseActivity {

    private Handler handler = new android.os.Handler();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handler.removeCallbacks(runnable);
        handler.postDelayed(runnable, 3000);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_splash_screen;
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            LoginActivity.navigate(SplashScreenActivity.this);
            finish();
        }
    };
}
