package com.example.aldhiramdan.yumlytest.network.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by aldhiramdan on 3/14/17.
 */

public class FoodDetailResponse implements Parcelable {

    @SerializedName("matches")
    @Expose
    public List<Matches> data;

    protected FoodDetailResponse(Parcel in) {
        data = in.createTypedArrayList(Matches.CREATOR);
    }

    public static final Creator<FoodDetailResponse> CREATOR = new Creator<FoodDetailResponse>() {
        @Override
        public FoodDetailResponse createFromParcel(Parcel in) {
            return new FoodDetailResponse(in);
        }

        @Override
        public FoodDetailResponse[] newArray(int size) {
            return new FoodDetailResponse[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(data);
    }


    public static class Matches implements Parcelable {

        public Matches() {
            //public constructor
        }

        @SerializedName("imageUrlsBySize")
        @Expose
        public ImageUrlBySize imageUrlsBySize;
        @SerializedName("sourceDisplayName")
        @Expose
        public String sourceDisplayName;
        @SerializedName("ingredients")
        @Expose
        public List<String> ingredients;
        @SerializedName("id")
        @Expose
        public String id;
        @SerializedName("smallImageUrls")
        @Expose
        public List<String> smallImageUrls;
        @SerializedName("recipeName")
        @Expose
        public String recipeName;
        @SerializedName("totalTimeInSeconds")
        @Expose
        public int totalTimeInSeconds;
        @SerializedName("attributes")
        @Expose
        public AttributesResponse attributes;
        @SerializedName("flavors")
        @Expose
        public FlavorsResponse flavors;
        @SerializedName("rating")
        @Expose
        public int rating;

        protected Matches(Parcel in) {
            sourceDisplayName = in.readString();
            ingredients = in.createStringArrayList();
            id = in.readString();
            smallImageUrls = in.createStringArrayList();
            recipeName = in.readString();
            totalTimeInSeconds = in.readInt();
            rating = in.readInt();
        }

        public static final Creator<Matches> CREATOR = new Creator<Matches>() {
            @Override
            public Matches createFromParcel(Parcel in) {
                return new Matches(in);
            }

            @Override
            public Matches[] newArray(int size) {
                return new Matches[size];
            }
        };

        @Override
        public String toString() {
            return "Matches{" +
                    "imageUrlsBySize=" + imageUrlsBySize +
                    ", sourceDisplayName='" + sourceDisplayName + '\'' +
                    ", ingredients=" + ingredients +
                    ", id='" + id + '\'' +
                    ", smallImageUrls=" + smallImageUrls +
                    ", recipeName='" + recipeName + '\'' +
                    ", totalTimeInSeconds=" + totalTimeInSeconds +
                    ", attributes=" + attributes +
                    ", flavors=" + flavors +
                    ", rating=" + rating +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Matches matches = (Matches) o;

            if (totalTimeInSeconds != matches.totalTimeInSeconds) return false;
            if (rating != matches.rating) return false;
            if (imageUrlsBySize != null ? !imageUrlsBySize.equals(matches.imageUrlsBySize) : matches.imageUrlsBySize != null)
                return false;
            if (sourceDisplayName != null ? !sourceDisplayName.equals(matches.sourceDisplayName) : matches.sourceDisplayName != null)
                return false;
            if (ingredients != null ? !ingredients.equals(matches.ingredients) : matches.ingredients != null)
                return false;
            if (id != null ? !id.equals(matches.id) : matches.id != null) return false;
            if (smallImageUrls != null ? !smallImageUrls.equals(matches.smallImageUrls) : matches.smallImageUrls != null)
                return false;
            if (recipeName != null ? !recipeName.equals(matches.recipeName) : matches.recipeName != null)
                return false;
            if (attributes != null ? !attributes.equals(matches.attributes) : matches.attributes != null)
                return false;
            return flavors != null ? flavors.equals(matches.flavors) : matches.flavors == null;

        }

        @Override
        public int hashCode() {
            int result = imageUrlsBySize != null ? imageUrlsBySize.hashCode() : 0;
            result = 31 * result + (sourceDisplayName != null ? sourceDisplayName.hashCode() : 0);
            result = 31 * result + (ingredients != null ? ingredients.hashCode() : 0);
            result = 31 * result + (id != null ? id.hashCode() : 0);
            result = 31 * result + (smallImageUrls != null ? smallImageUrls.hashCode() : 0);
            result = 31 * result + (recipeName != null ? recipeName.hashCode() : 0);
            result = 31 * result + totalTimeInSeconds;
            result = 31 * result + (attributes != null ? attributes.hashCode() : 0);
            result = 31 * result + (flavors != null ? flavors.hashCode() : 0);
            result = 31 * result + rating;
            return result;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(sourceDisplayName);
            dest.writeStringList(ingredients);
            dest.writeString(id);
            dest.writeStringList(smallImageUrls);
            dest.writeString(recipeName);
            dest.writeInt(totalTimeInSeconds);
            dest.writeInt(rating);
        }
    }


    public static class ImageUrlBySize implements Parcelable {
        @SerializedName("90")
        @Expose
        public String ninety;

        protected ImageUrlBySize(Parcel in) {
            ninety = in.readString();
        }

        public static final Creator<ImageUrlBySize> CREATOR = new Creator<ImageUrlBySize>() {
            @Override
            public ImageUrlBySize createFromParcel(Parcel in) {
                return new ImageUrlBySize(in);
            }

            @Override
            public ImageUrlBySize[] newArray(int size) {
                return new ImageUrlBySize[size];
            }
        };

        @Override
        public String toString() {
            return "ImageUrlBySize{" +
                    "ninety='" + ninety + '\'' +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            ImageUrlBySize that = (ImageUrlBySize) o;

            return ninety != null ? ninety.equals(that.ninety) : that.ninety == null;

        }

        @Override
        public int hashCode() {
            return ninety != null ? ninety.hashCode() : 0;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(ninety);
        }
    }
}
