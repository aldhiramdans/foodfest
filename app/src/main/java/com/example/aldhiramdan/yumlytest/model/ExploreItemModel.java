package com.example.aldhiramdan.yumlytest.model;

import android.graphics.drawable.Drawable;
import android.widget.TextView;

/**
 * Created by aldhiramdan on 3/22/17.
 */

public class ExploreItemModel {

    public Drawable imageItem;
    public Drawable logoItem;
    public String titleItem;

    public Drawable getImageItem() {
        return imageItem;
    }

    public void setImageItem(Drawable imageItem) {
        this.imageItem = imageItem;
    }

    public Drawable getLogoItem() {
        return logoItem;
    }

    public void setLogoItem(Drawable logoItem) {
        this.logoItem = logoItem;
    }

    public String getTitleItem() {
        return titleItem;
    }

    public void setTitleItem(String titleItem) {
        this.titleItem = titleItem;
    }
}
