package com.example.aldhiramdan.yumlytest.presenter;

/**
 * Created by aldhiramdan on 4/3/17.
 */

public interface DietPresenter {

    void getFoodDiet();

    void cancelRequest();

}
