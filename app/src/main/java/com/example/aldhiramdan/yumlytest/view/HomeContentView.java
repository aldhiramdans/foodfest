package com.example.aldhiramdan.yumlytest.view;

import com.example.aldhiramdan.yumlytest.network.response.FoodDetailResponse;

import java.util.List;

/**
 * Created by aldhiramdan on 3/17/17.
 */

public interface HomeContentView {

    void onGetFoodDetailSuccess(List<FoodDetailResponse.Matches> response);

    void onGetFoodDetailFailure(Throwable throwable);
}
