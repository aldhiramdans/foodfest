package com.example.aldhiramdan.yumlytest.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by aldhiramdan on 3/30/17.
 */

public class RighteousRegularTextView extends TextView {
    public RighteousRegularTextView(Context context) {
        super(context);
        init(null, 0);
    }

    public RighteousRegularTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public RighteousRegularTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs, defStyleAttr);
    }

    private void init(AttributeSet attrs, int defStyle) {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Righteous-Regular.ttf");
        setTypeface(tf, defStyle);
    }
}
