package com.example.aldhiramdan.yumlytest.helper.preference;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.aldhiramdan.yumlytest.helper.util.CrypthoUtil;

/**
 * Created by aldhiramdan on 3/29/17.
 */

public class AppSharedPreference {

    public static final String PREF_NAME = "pref_food_app";

    private Context mContext;
    private ObsecureSharedPreference mObscuredSharedPreferences;

    private static final Object LOCK = new Object();
    private static AppSharedPreference sInstance;

    public static AppSharedPreference getInstance() {
        synchronized (LOCK) {
            if (sInstance == null) {
                sInstance = new AppSharedPreference();
            }
        }
        return sInstance;
    }

    public AppSharedPreference() {
        this.mContext = null;
        SharedPreferences preferences = null;
        if (mContext != null) {
            preferences = mContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        }
        mObscuredSharedPreferences = new ObsecureSharedPreference(mContext, preferences, CrypthoUtil.getSecureId(mContext));
    }

    public void set(String key, String value) {
        mObscuredSharedPreferences.edit().putString(key, value).apply();
    }

    public void set(String key, int value) {
        mObscuredSharedPreferences.edit().putInt(key, value).apply();
    }

    public void set(String key, boolean value) {
        mObscuredSharedPreferences.edit().putBoolean(key, value).apply();
    }

    public void set(String key, float value) {
        mObscuredSharedPreferences.edit().putFloat(key, value).apply();
    }

    public String getString(String key) {
        return mObscuredSharedPreferences.getString(key, "");
    }

    public boolean getBoolean(String key) {
        return mObscuredSharedPreferences.getBoolean(key, false);
    }

    public int getInt(String key) {
        return mObscuredSharedPreferences.getInt(key, 0);
    }


}
