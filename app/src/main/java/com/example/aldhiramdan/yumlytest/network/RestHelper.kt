package com.example.aldhiramdan.yumlytest.network

import com.example.aldhiramdan.yumlytest.BuildConfig
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by aldhiramdan on 3/15/17.
 */
class RestHelper {

    companion object {
        val instance by lazy { RestHelper() }
    }

    fun service(): RestService {
        return retrofit().create(RestService::class.java)
    }

    private fun retrofit(): Retrofit {
        return Retrofit.Builder()
                .baseUrl(ApiConstant.BASE_URL)
                .client(okHttp())
                .addConverterFactory(GsonConverterFactory.create(buildGson()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
    }

    private fun buildGson(): Gson = GsonBuilder()
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
            .create()

    private fun okHttp(): OkHttpClient {
        return OkHttpClient.Builder()
                .addInterceptor(HttpLoggingInterceptor().apply {
                    level = if (BuildConfig.DEBUG)
                        HttpLoggingInterceptor.Level.BODY
                    else
                        HttpLoggingInterceptor.Level.NONE
                })
                .connectTimeout(ApiConstant.CONNECTION_TIMEOUT, TimeUnit.MILLISECONDS)
                .readTimeout(ApiConstant.READ_TIMEOUT, TimeUnit.MILLISECONDS)
                .build()
    }

}