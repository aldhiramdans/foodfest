package com.example.aldhiramdan.yumlytest.view;

/**
 * Created by aldhiramdan on 4/3/17.
 */

public interface DietView {

    void getFoodDietSuccess();

    void getFoodDietFailure();
}
