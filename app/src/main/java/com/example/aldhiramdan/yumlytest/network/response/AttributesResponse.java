package com.example.aldhiramdan.yumlytest.network.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by aldhiramdan on 3/20/17.
 */

public class AttributesResponse implements Parcelable{

    public AttributesResponse() {

    }

    @SerializedName("course")
    @Expose
    public List<String> courses;
    @SerializedName("cuisine")
    @Expose
    public List<String> cuisine;
    @SerializedName("holiday")
    @Expose
    public List<String> holiday;

    protected AttributesResponse(Parcel in) {
        courses = in.createStringArrayList();
        cuisine = in.createStringArrayList();
        holiday = in.createStringArrayList();
    }

    public static final Creator<AttributesResponse> CREATOR = new Creator<AttributesResponse>() {
        @Override
        public AttributesResponse createFromParcel(Parcel in) {
            return new AttributesResponse(in);
        }

        @Override
        public AttributesResponse[] newArray(int size) {
            return new AttributesResponse[size];
        }
    };

    @Override
    public String toString() {
        return "AttributesResponse{" +
                "courses=" + courses +
                ", cuisine=" + cuisine +
                ", holiday=" + holiday +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AttributesResponse that = (AttributesResponse) o;

        if (courses != null ? !courses.equals(that.courses) : that.courses != null) return false;
        if (cuisine != null ? !cuisine.equals(that.cuisine) : that.cuisine != null) return false;
        return holiday != null ? holiday.equals(that.holiday) : that.holiday == null;

    }

    @Override
    public int hashCode() {
        int result = courses != null ? courses.hashCode() : 0;
        result = 31 * result + (cuisine != null ? cuisine.hashCode() : 0);
        result = 31 * result + (holiday != null ? holiday.hashCode() : 0);
        return result;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringList(courses);
        dest.writeStringList(cuisine);
        dest.writeStringList(holiday);
    }
}
