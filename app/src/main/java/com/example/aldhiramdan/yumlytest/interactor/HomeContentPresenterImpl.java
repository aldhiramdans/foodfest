package com.example.aldhiramdan.yumlytest.interactor;

import android.content.Context;

import com.example.aldhiramdan.yumlytest.network.response.FoodDetailResponse;
import com.example.aldhiramdan.yumlytest.network.RestHelper;
import com.example.aldhiramdan.yumlytest.network.RestService;
import com.example.aldhiramdan.yumlytest.presenter.HomeContentPresenter;
import com.example.aldhiramdan.yumlytest.view.HomeContentView;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by aldhiramdan on 3/17/17.
 */

public class HomeContentPresenterImpl implements HomeContentPresenter {

    private RestService service = RestHelper.Companion.getInstance().service();

    private Context mContext;
    private HomeContentView mView;
    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    public HomeContentPresenterImpl(Context context, HomeContentView view) {
        this.mContext = context;
        this.mView = view;
    }

    @Override
    public void getFoodDetail(int rowStart, int maxRow) {
        int startRow = 0;
        Disposable disposable = service.getFoodDetail(maxRow, startRow)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<FoodDetailResponse>() {
                    @Override
                    public void accept(FoodDetailResponse response) throws Exception {
                        mView.onGetFoodDetailSuccess(response.data);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        mView.onGetFoodDetailFailure(throwable);
                    }
                });
        mCompositeDisposable.add(disposable);
    }

    @Override
    public void onDestroy() {
        mCompositeDisposable.clear();
    }
}
