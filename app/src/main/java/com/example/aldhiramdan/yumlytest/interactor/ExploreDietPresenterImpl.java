package com.example.aldhiramdan.yumlytest.interactor;

import com.example.aldhiramdan.yumlytest.network.RestHelper;
import com.example.aldhiramdan.yumlytest.network.RestService;
import com.example.aldhiramdan.yumlytest.network.response.FoodDetailResponse;
import com.example.aldhiramdan.yumlytest.presenter.DietPresenter;
import com.example.aldhiramdan.yumlytest.view.DietView;

import java.util.List;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by aldhiramdan on 4/3/17.
 */

public class ExploreDietPresenterImpl implements DietPresenter {

    private RestService service = RestHelper.Companion.getInstance().service();

    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    private List<FoodDetailResponse.Matches> foods;
    private DietView mVIew;

    public ExploreDietPresenterImpl(List<FoodDetailResponse.Matches> food, DietView view) {
        this.foods = food;
        this.mVIew = view;
    }

    @Override
    public void getFoodDiet() {

    }

    @Override
    public void cancelRequest() {

    }
}
