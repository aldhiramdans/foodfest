package com.example.aldhiramdan.yumlytest.helper.util;

import android.graphics.Color;
import android.support.v4.widget.SwipeRefreshLayout;

import com.example.aldhiramdan.yumlytest.R;

/**
 * Created by aldhiramdan on 3/27/17.
 */

public class WidgetUtil {

    public static void swipeRefresh(SwipeRefreshLayout swipeRefresh) {
        int myColor = Color.parseColor("#CCCCCC");
        swipeRefresh.setProgressBackgroundColorSchemeColor(myColor);
        swipeRefresh.setColorSchemeResources(R.color.colorGeneralTextDark);
    }

}
