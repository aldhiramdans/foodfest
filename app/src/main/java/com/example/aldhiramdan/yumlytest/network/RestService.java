package com.example.aldhiramdan.yumlytest.network;


import com.example.aldhiramdan.yumlytest.network.response.FoodDetailResponse;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

/**
 * Created by aldhiramdan on 3/16/17.
 */

public interface RestService {

    @Headers({
            "X-Yummly-App-ID: d6d6741a",
            "X-Yummly-App-Key: 5c18c7197cf66b848da1a7bdb42db95e"
    })
    @GET(ApiConstant.FOOD_LIST)
    Observable<FoodDetailResponse> getFoodDetail(
            @Query("maxResult") int maxResult,
            @Query("start") int start
    );

    @Headers({
            "X-Yummly-App-ID: d6d6741a",
            "X-Yummly-App-Key: 5c18c7197cf66b848da1a7bdb42db95e"
    })
    @GET(ApiConstant.FOOD_LIST)
    Call<FoodDetailResponse> searchFood(
            @Query("q") String recipeName,
            @Query("maxResult") int maxResult,
            @Query("start") int start
    );

    @Headers({
            "X-Yummly-App-ID: d6d6741a",
            "X-Yummly-App-Key: 5c18c7197cf66b848da1a7bdb42db95e"
    })
    @GET(ApiConstant.FOOD_DIET)
    Observable<FoodDetailResponse> dietFood(
            @Query("maxResult") int maxResult,
            @Query("start") int start
    );
}
